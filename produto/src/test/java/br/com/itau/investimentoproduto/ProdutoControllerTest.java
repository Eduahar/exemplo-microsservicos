package br.com.itau.investimentoproduto;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.itau.investimentoproduto.models.Produto;
import br.com.itau.investimentoproduto.services.ProdutoService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProdutoControllerTest {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	ProdutoService produtoService;
	
	@Test
	public void buscarClientes() throws Exception {
		Produto produto = new Produto();
		produto.setId(1);
		
		when(produtoService.buscar(1)).thenReturn(Optional.of(produto));
		
		mockMvc.perform(get("/1"))
			.andExpect(content().string(containsString("1")));
	}
}
